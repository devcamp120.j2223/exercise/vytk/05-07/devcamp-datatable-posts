
import * as React from 'react';
import { Container,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,Paper,Grid,Button,Pagination } from "@mui/material";
import { useEffect, useState } from "react";
export default function DataTable(){
    const [row,setRows]=useState([]);
    
    //Limit: Số lượng bản ghi trên 1 trang
    const limit=10;

    //Số trang: Tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
    const [countPage,setcountPage]=useState(0);

    //Trang hiện tại:
    const [page,setPage]=useState(1);

    const onChangePagination=(event,value)=>{
        setPage(value);
    }
    const callAPI=async(url)=>{
        const res = await fetch(url);
        const data = await res.json();

        return data;
    }
    const clickHandler=(data)=>{
        console.log(data);  
    }

    useEffect(()=>{
        callAPI('https://jsonplaceholder.typicode.com/posts')
            .then((data)=>{
                setcountPage(Math.ceil(data.length/limit))
                setRows(data.slice((page-1)*limit,page*limit))
            })
            .catch((err)=>{console.error(err.message)})
    },[page])
    return(
        <Container>
            <Grid container>
                <Grid item>
                    <TableContainer component={Paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align='center'>ID</TableCell>
                                    <TableCell align='center'>User ID</TableCell>
                                    <TableCell align='center'>Title</TableCell>
                                    <TableCell align='center'>Body</TableCell>
                                    <TableCell align='center'>Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    row.map((row,index)=>{
                                        return(
                                            <TableRow key={index}>
                                                <TableCell align='center'>{row.id}</TableCell>
                                                <TableCell align='center'>{row.userId}</TableCell>
                                                <TableCell >{row.title}</TableCell>
                                                <TableCell >{row.body}</TableCell>
                                                <TableCell align='center'><Button variant='contained' onClick={()=>{clickHandler(row)}}>Chi Tiết</Button></TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={3} justifyContent='flex-end'>
                <Grid item>
                    <Pagination count={countPage} defaultPage={page} onChange={onChangePagination}/>  
                </Grid>
            </Grid>
        </Container>
    )
}